package com.example.swagger;


/**
 * @author lirong
 * @ClassName: ResultCode
 * @Description: 返回的状态码
 * @date 2018-12-03 9:26
 */

public enum GlobalResultEnum {

    /**
     * HTTP接口通信相关
     */
    FAIL(0, "操作失败"),

    SUCCESS(200, "操作成功"),

    /**
     * 账号相关
     */
    USER_NOT_EXIST(100, "该用户不存在"),

    PHONE_UNLIKE_BIND(101, "手机号非当前用户绑定手机"),

    TWICE_PASSWORD_DISAGREED(102, "两次输入密码不一致"),

    ACCOUNT_DISABLED(103, "账号已被禁用"),

    UPDATE_INIT_PWD(104, "请修改初始密码"),

    USER_PASS_ERRO(105, "账户或密码错误"),

    /**
     * 资源相关
     */
    NOT_FOUND(404, "找不到资源"),

    FORBIDDEN(403, "没有访问权限"),

    UNAUTHORIZED(401, "未认证,请登录后操作"),

    FAIL_AUTHORIZED(405, "认证失败"),

    /**
     * 应用相关
     */
    ERROR(500, "服务器内部错误"),

    APPLY_STOP(502, "应用已停止运行"),


    /**
     * 会话相关
     */
    TOKEN_EXPIRE(901, "令牌过期或无效,请重新获取"),
    TOKEN_PARSE_ERROR(902, "令牌解析错误，请检查令牌是否正确"),
    REF_TOKEN_EXPIRE(903, "刷新令牌过期或无效,请重新登录"),
    LOGIN_EXPIRE(999, "登陆过期,请重新登陆"),
    ;


    public int code;

    public String message;

    GlobalResultEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


}
