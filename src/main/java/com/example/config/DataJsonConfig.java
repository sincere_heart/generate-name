package com.example.config;

import com.example.core.Tools;
import com.example.tools.JsonUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * @author liheng
 * @ClassName DataJsonConfig
 * @Description
 * @date 2021-07-31 19:14
 */
@Configuration
public class DataJsonConfig {
    @Value("classpath:origin/AZ_1_5_NAME_H-SIX_NAME-PG_1_5_NAME_H.json")
    private Resource az15NameHSixNamePg15NameJson;

    public static Map<String, String> az15NameHSixNamePg15NameHmap;

    @PostConstruct
    public void init() {
        az15NameHSixNamePg15NameHmap = az15NameHSixNamePg15NameHmap();
    }

    @Bean
    public Map<String, String> az15NameHSixNamePg15NameHmap() {
        String data = Tools.readJsonData(az15NameHSixNamePg15NameJson);
        Map<String, String> textTransforms = JsonUtils.parseObject(data, new TypeReference<Map<String, String>>() {
        });
        return textTransforms;
    }
}
