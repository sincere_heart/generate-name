package com.example.config;

import com.example.cache.Cache;
import com.example.cache.CaffineCache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author liheng
 * @ClassName AccessTokenConfig
 * @Description
 * @date 2021-01-02 19:56
 */
@Configuration
public class AccessTokenConfig {
    @Value("${token.env:'none'}")
    private String env;
    @Value("${token.tokenExpireTime:7200}")
    private long tokenExpireTime;
    @Value("${token.refreshTokenExpireTime:86400}")
    private long refreshTokenExpireTime;

    /**
     * 令牌缓存
     *
     * @return
     */
    @Bean
    public CaffineCache<String> accessTokenCache() {
        Cache cache = Cache.options().setDuration(tokenExpireTime).setTimeUnit(TimeUnit.SECONDS).build();
        return new CaffineCache(env, cache);
    }

}
