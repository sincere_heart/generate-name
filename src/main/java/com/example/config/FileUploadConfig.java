package com.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>文件上传配置</p>
 *
 * @author mouseyCat
 * @date 2020/10/13 16:10
 */
@Data
@Component
@ConfigurationProperties(prefix = "file.upload")
public class FileUploadConfig {
    private String accessPath;
    private String allowExt;
    private String location;
}
