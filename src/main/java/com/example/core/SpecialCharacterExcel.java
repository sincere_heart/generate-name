package com.example.core;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * @author liheng
 * @ClassName SpecialCharacterExcel
 * @Description
 * @date 2021-07-28 16:46
 */
public class SpecialCharacterExcel {
    @Excel(name = "A1")
    private String a1;
    @Excel(name = "A2")
    private String a2;
    @Excel(name = "A3")
    private String a3;
    @Excel(name = "A4")
    private String a4;


    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getA4() {
        return a4;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }


    @Override
    public String toString() {
        return "SpecialCharacterExcel{" +
                "a1='" + a1 + '\'' +
                ", a2='" + a2 + '\'' +
                ", a3='" + a3 + '\'' +
                ", a4='" + a4 + '\'' +
                '}';
    }
}
