package com.example.core;

import com.example.config.DataJsonConfig;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author liheng
 * @ClassName GenerateNameTools
 * @Description
 * @date 2021-07-31 21:55
 */
public class GenerateNameTools {
    /**
     * 名字最大长度
     */
    public static final int MAX_LEN = 5;
    /**
     * 1.1-5个字换字难改苹果思路
     * <p>
     * 比如输入的两个字 自动排查哪个字有没有有的话自动更换掉那个字  然后从数据库2中选3个  然后数据库3中就一个符号 然后加两个同样的符号
     * 比如输入的三个字 自动排查哪个字有没有有的话自动更换掉那个字  然后从数据库2中选2个  然后数据库3中就一个符号 然后加两个同样的符号
     * <p>
     * 比如输入的四个字 自动排查哪个字有没有有的话自动更换掉那个字  然后从数据库2中选1个  然后数据库3中就一个符号 然后加两个同样的符号
     * 比如输入的五个字 自动排查哪个字有没有有的话自动更换掉那个字   然后数据库3中就一个符号 然后加两个同样的符号
     */
    private static String PG_1_5_NAME_H_STR_2 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";
    private static String PG_1_5_NAME_H_STR_3 = "\u007F";

    /**
     * 1.1-5个字换字难改苹果思路
     *
     * @param inputName
     * @return
     */
    public static Map<String, String> generateNameByPG_1_5_NAME_H(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 2 && inputName.length() <= MAX_LEN) {
            Map<String, String> pg15NameHmap = DataJsonConfig.az15NameHSixNamePg15NameHmap;
            Set<String> set = pg15NameHmap.keySet();
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> tempUnicodeNames = new ArrayList<>();
            for (String input : inputNames) {
                if (set.contains(input)) {
                    tempUnicodeNames.add(Tools.string2Unicode(pg15NameHmap.get(input)));
                } else {
                    tempUnicodeNames.add(Tools.string2Unicode(input));
                }
            }
            // 获取库2
            List<String> specials = Tools.specials2List(PG_1_5_NAME_H_STR_2);
            // 乱序
            Collections.shuffle(specials);
            // 挑选字符
            List<String> newSpecials = Tools.getStringByRadom(specials, MAX_LEN - inputNames.size());
            String unicode = Tools.string2Unicode(PG_1_5_NAME_H_STR_3);
            // 追加库3
            newSpecials.add(unicode);
            newSpecials.add(unicode);
            // 乱序
            Collections.shuffle(newSpecials);
            // 位置改变
            return Tools.positionChange(tempUnicodeNames, newSpecials);
        } else {
            throw new RuntimeException("输入的字数在2-5位");
        }
    }


    /**
     * 2.1-5个字难改换字安卓
     * 比如输入2个字 系统就要排查 这两个字咱们有哪个字 进行更换 数据中有字 更换后从数据库3中随机选一个家进入然后再从数据库2中选三个
     * 比如输入三个字 系统就要排查这三个字 咱们有哪个字 进行更换 然后从数据库三中选一个随机加入 然后再冲数据库2中选2个
     * 四个字  系统就要排查这四个字咱们有哪个字 然后进行更换 然后从数据库三中选一个加入然后咋子冲数据库2中选一个
     * 如果五个字 排查哪个字可以换 然后换完了以后 从数据库3中随机选一个加入进去即可
     */
    private static String AZ_1_5_NAME_H_STR_2 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";
    private static String AZ_1_5_NAME_H_STR_3 = "\u0FDC\u0FDD\u0FDE\u0FDF\u0FE0\u0FE1\u0FE2\u0FE3\u0FE4\u0FE5\u0FE6\u0FE7\u0FE8\u0FE9\u0FEA\u0FEB\u0FEC\u0FED\u0FEE\u0FEF\u0FF0\u0FF1\u0FF2\u0FF3\u0FF4\u0FF5\u0FF6\u0FF7\u2067\u2069";

    /**
     * 1-5个字难改换字安卓
     *
     * @param inputName
     * @return
     */
    public static Map<String, String> generateNameByAZ_1_5_NAME_H(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 2 && inputName.length() <= MAX_LEN) {
            Map<String, String> az15NameHmap = DataJsonConfig.az15NameHSixNamePg15NameHmap;
            Set<String> set = az15NameHmap.keySet();
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> tempUnicodeNames = new ArrayList<>();
            for (String input : inputNames) {
                if (set.contains(input)) {
                    tempUnicodeNames.add(Tools.string2Unicode(az15NameHmap.get(input)));
                } else {
                    tempUnicodeNames.add(Tools.string2Unicode(input));
                }
            }
            // 获取库2
            List<String> specials2 = Tools.specials2List(AZ_1_5_NAME_H_STR_2);
            // 乱序
            Collections.shuffle(specials2);
            // 挑选字符
            List<String> newSpecials2 = Tools.getStringByRadom(specials2, MAX_LEN - inputNames.size());

            // 追加库3
            List<String> specials3 = Tools.specials2List(AZ_1_5_NAME_H_STR_3);
            // 乱序
            Collections.shuffle(specials3);
            // 挑选字符
            List<String> newSpecials3 = Tools.getStringByRadom(specials3, 1);
            newSpecials2.addAll(newSpecials3);
            // 位置改变
            return Tools.positionChange(tempUnicodeNames, newSpecials2);
        } else {
            throw new RuntimeException("输入的字数在2-5位");
        }
    }


    /**
     * 3.1-5个字难改名字安卓思路
     * 例如输入的是一个字 就从数据库1中随机挑选一个 然后再从数据库2中随机挑选4个 正好是1+1+4=6       位置都是随机 符号顺序也是随机
     * 如果是两个字 就从数据库1中随机挑选1个 然后再从数据库2中随机挑选三个 正好是2+1+3                   位置都是随机 符号顺序也是随机 字的顺序不能颠倒 比如我的 不能变成的我 但是中间可以加比如     符号我符号符号的符号
     * 如果是三个字 就从数据库1中随机挑选1个 然后再冲数据库2中随机挑选2个 正好是3+1+2
     * 如果是四个字  就从数据库1中挑选一个 然后从2中挑选1个  4+1+1
     * 如果五个字  就从数据库中挑选一个 即可数据库2中 不挑选
     * 上面随机插入 都可以插入任何位置跟 地方只要不改变名字的顺序
     */
    private static String AZ_1_5_NAME_STR_1 = "\u2067\u2069\u0FDC\u0FDD\u0FDE\u0FDF\u0FE0\u0FE1\u0FE2\u0FE3\u0FE4\u0FE5\u0FE6\u0FE7\u0FE8\u0FE9\u0FEA\u0FEB\u0FEC\u0FED\u0FEE\u0FEF\u0FF0\u0FF1\u0FF2\u0FF3\u0FF4\u0FF5\u0FF6\u0FF7";
    private static String AZ_1_5_NAME_STR_2 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";

    /**
     * 1-5个字难改名字安卓思路
     *
     * @param inputName
     * @return
     */
    public static Map<String, String> generateNameByAZ_1_5_NAME(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 1 && inputName.length() <= MAX_LEN) {
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> tempUnicodeNames = new ArrayList<>();
            for (String input : inputNames) {
                tempUnicodeNames.add(Tools.string2Unicode(input));
            }
            // 获取库1
            List<String> specials1 = Tools.specials2List(AZ_1_5_NAME_STR_1);
            // 乱序
            Collections.shuffle(specials1);
            // 挑选字符
            List<String> newSpecials1 = Tools.getStringByRadom(specials1, 1);

            // 追加库2
            List<String> specials2 = Tools.specials2List(AZ_1_5_NAME_STR_2);
            // 乱序
            Collections.shuffle(specials2);
            // 挑选字符
            List<String> newSpecials2 = Tools.getStringByRadom(specials2, MAX_LEN - inputNames.size());
            newSpecials1.addAll(newSpecials2);
            // 位置改变
            return Tools.positionChange(tempUnicodeNames, newSpecials1);

        } else {
            throw new RuntimeException("输入的字数在1-5位");
        }
    }

    /**
     * 4.1-5个字难改名字苹果思路
     * 数据库2是就一个符号
     * 例如输入的是一个字 自动从数据库1中挑选4个然后+2个数据库2中的符号  随机插入位置随机不改变名字顺序就行
     * 如果是2个字 从数据库1中挑选3个然后+2个数据库2中的符  随机插入位置随机不改变名字顺序就行
     * 三个 四个 都是一样
     * 五个字的话 就从数据库2中的一个符号 加两个进去随机顺序即可
     */
    private static String PG_1_5_NAME_STR_1 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";
    private static String PG_1_5_NAME_STR_2 = "\u007F";

    /**
     * 1-5个字难改名字苹果思路
     *
     * @param inputName
     * @return
     */
    public static Map<String, String> generateNameByPG_1_5_NAME(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 1 && inputName.length() <= MAX_LEN) {
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> tempUnicodeNames = new ArrayList<>();
            for (String input : inputNames) {
                tempUnicodeNames.add(Tools.string2Unicode(input));
            }
            // 获取库1
            List<String> specials1 = Tools.specials2List(PG_1_5_NAME_STR_1);
            // 乱序
            Collections.shuffle(specials1);
            // 挑选字符
            List<String> newSpecials1 = Tools.getStringByRadom(specials1, MAX_LEN - inputNames.size());
            String unicode = Tools.string2Unicode(PG_1_5_NAME_STR_2);
            // 追加库3
            newSpecials1.add(unicode);
            newSpecials1.add(unicode);
            Collections.shuffle(specials1);
            // 位置改变
            return Tools.positionChange(tempUnicodeNames, newSpecials1);
        } else {
            throw new RuntimeException("输入的字数在1-5位");
        }
    }

    /**
     * 5.6个字名字
     * 比如残留一抹幽香
     * 这六个字的名字
     * 咱们有两个字 一 跟香有代码自
     * 点击生成后
     * 残留代码字抹幽香
     * 残留代码字一抹幽代码字香
     * 残留一抹幽代码字香
     * 自动生成三个
     * 如果就一个字有 就自动生成一个 即可 如果有三个组合就自动生成三个
     * 如果有四个组合就自动生成四个
     */
    public static List<Map<String, String>> generateNameBySIX_NAME(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() == MAX_LEN + 1) {
            List<Map<String, String>> resultList = new ArrayList<>();
            Map<String, String> sixNamemap = DataJsonConfig.az15NameHSixNamePg15NameHmap;
            Set<String> set = sixNamemap.keySet();
            List<String> inputNames = Arrays.asList(inputName.split(""));
            // 查找替换的个数
            int count = (int) inputNames.stream().filter(str -> sixNamemap.keySet().contains(str)).count();
            if (count != 0L) {
                List<String> tempResult = new ArrayList<>();
                int[] dataArrTemp = new int[count];
                // 列出所有替换可能，0 不替换，1替换
                List<String> result = Tools.zeroOneGen(dataArrTemp, 0, tempResult);
                System.out.println(result.toString());
                Map<String, String> map;
                for (String str : result) {
                    map = new HashMap<>(2);
                    String resultStr = "";
                    int i = 0;
                    for (String input : inputNames) {
                        resultStr = resultStr + input;
                        if (set.contains(input)) {
                            // 替换
                            if (str.substring(i, i + 1).equals("1")) {
                                resultStr = resultStr.replace(input, sixNamemap.get(input));
                            }
                            i++;
                        }
                    }
                    String unicode = Tools.string2Unicode(resultStr);
                    map.put("unicode", unicode);
                    map.put("value", resultStr);
                    resultList.add(map);
                }
            } else {
                Map<String, String> map = new HashMap<>(2);
                String unicode = Tools.string2Unicode(inputName);
                map.put("unicode", unicode);
                map.put("value", inputName);
                resultList.add(map);
            }
            return resultList;
        } else {
            throw new RuntimeException("输入的字数必须在6位");
        }
    }


    /**
     * 6.安卓贵族居中
     * 安卓贵族居中 也是不需要输入任何东西就会自动生成的
     * 从数据库1当中随机挑选6个即可 顺序要错乱即可
     */
    private static String AZ_GZ_STR = "\u0FDC\u0FDD\u0FDE\u0FDF\u0FE0\u0FE1\u0FE2\u0FE3\u0FE4\u0FE5\u0FE6\u0FE7\u0FE8\u0FE9\u0FEA\u0FEB\u0FEC\u0FED\u0FEE\u0FEF\u0FF0\u0FF1\u0FF2\u0FF3\u0FF4\u0FF5\u0FF6\u0FF7";

    /**
     * 安卓贵族居中
     *
     * @return
     */
    public static Map<String, String> generateNameByAZ_GZ() {
        // 获取库
        List<String> specials = Tools.specials2List(AZ_GZ_STR);
        // 乱序
        Collections.shuffle(specials);
        // 挑选字符
        List<String> newSpecials1 = Tools.getStringByRadom(specials, MAX_LEN + 1);
        // 乱序
        Collections.shuffle(newSpecials1);
        Map<String, String> map = new HashMap<>(2);
        String unicode = Joiner.on("").join(newSpecials1);
        String s1 = Tools.unicode2String(unicode);
        map.put("unicode", unicode);
        map.put("value", s1);
        // 位置改变
        return map;
    }


    /**
     * 7.苹果贵族居中思路
     * 苹果贵族居中也是一样的不需要输入任何东西直接点生成即可
     * 数据库2中就一个符号
     * 首先从数据库2中随机2-8个 2个也行3个也行
     * 数据库2中的 算半个字符 比如第一步从数据库二中随机加了两个 就是算一个
     * 然后再从数据1中挑选5个 随机排序随机位置 填充够6个即可
     * 比如从数据库2中随机挑选了三个 就是1个半 默认为2个再冲 数据库1中随机选4个 随机位置随机顺序 即可
     * 比如从数据库中挑选了4个 就是2个字也是从数据库中选4个 随机位置随机顺序 即可
     * 比如从数据库中挑选了5个 就是2个半字 然后默认为三个字 再从数据库1中随机挑选3个 随机位置随机顺序 即可
     * 比如从数据库中挑选了6个 就是3字 然后默认为三个字 再从数据库1中随机挑选3个 随机位置随机顺序 即可
     */
    private static String PG_GZ_STR_1 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";
    private static String PG_GZ_STR_2 = "\u007F";

    /**
     * 苹果贵族居中
     * TODO 待确认
     *
     * @return
     */
    public static Map<String, String> generateNameByPG_GZ() {
        // 获取库
        List<String> specials = Tools.specials2List(PG_GZ_STR_2);
        int num = Tools.nextIntBh(2, 8);
        // 挑选字符
        List<String> newSpecials2 = Tools.getStringByRadom(specials, num);
        int len;
        if (num % 2 == 0) {
            len = num / 2;
        } else {
            len = (num / 2) + 1;
        }
        // 获取库
        List<String> specials1 = Tools.specials2List(PG_GZ_STR_1);
        // 乱序
        Collections.shuffle(specials1);
        // 挑选字符
        List<String> newSpecials1 = Tools.getStringByRadom(specials, MAX_LEN + 1 - len);
        newSpecials2.addAll(newSpecials1);
        // 乱序
        Collections.shuffle(newSpecials2);
        Map<String, String> map = new HashMap<>(2);
        String unicode = Joiner.on("").join(newSpecials2);
        String s1 = Tools.unicode2String(unicode);
        map.put("unicode", unicode);
        map.put("value", s1);
        // 位置改变
        return map;
    }


    /**
     * 8.空白名串源
     * 普通空白名是不需要输入任何东西就可以点击生成的
     * 思路
     * 1.从数据库1当中随机挑选三个
     * 2.根据第一步挑选的三个 再从数据库2里面随机挑选两个放在第一步从数据库1挑选的三个里面位置随机顺序随机
     * 3.之前两个步骤已经有5个符号了再从数据库三当中 数据库三就两个字符 把这两个随机穿插在这五个字符中即可
     */
    private static String KB_STR_1 = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";
    //public static String KB_STR_2 = "ㅤㅤㅤ ";
    private static String KB_STR_2 = "\u3164\u3164\u3164\u2005";
    private static String KB_STR_3 = "\u007F\u007F";

    /**
     * 苹果贵族居中
     *
     * @return
     */
    public static Map<String, String> generateNameByKB() {
        // 获取库
        List<String> specials1 = Tools.specials2List(KB_STR_1);
        // 乱序
        Collections.shuffle(specials1);
        // 挑选字符
        List<String> newSpecials1 = Tools.getStringByRadom(specials1, 3);

        // 获取库
        List<String> specials2 = Tools.specials2List(KB_STR_2);
        // 乱序
        Collections.shuffle(specials2);
        // 挑选字符
        List<String> newSpecials2 = Tools.getStringByRadom(specials2, 2);
        newSpecials1.addAll(newSpecials2);

        // 获取库
        List<String> specials3 = Tools.specials2List(KB_STR_3);
        // 乱序
        Collections.shuffle(specials3);
        // 挑选字符
        List<String> newSpecials3 = Tools.getStringByRadom(specials3, 2);
        newSpecials1.addAll(newSpecials3);
        // 乱序
        Collections.shuffle(newSpecials1);
        Map<String, String> map = new HashMap<>(2);
        String unicode = Joiner.on("").join(newSpecials1);
        String s1 = Tools.unicode2String(unicode);
        map.put("unicode", unicode);
        map.put("value", s1);
        // 位置改变
        return map;
    }

    /**
     * 9.重复名串源
     * 例如输入一个字系统自动随机选择5个数据库里面的符号 然后随机插入到字里面顺序随机
     * 但是随机性一定要好
     * 如果2个字 随机挑选4个
     * 三个字也是   随机挑选三
     * 四个字也是   随机挑选2
     * 五个字也是   随机挑选1
     * 最多6个字   超过六个字会提示 最多6个字超过无法更改
     * <p>
     * 流程 比如顾客要  我这个名字
     * 放入后自动生成 然后自动复制 清理
     */
    private static String CF_STR = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";

    /**
     * 重复名
     *
     * @return
     */
    public static Map<String, String> generateNameByCF(String inputName) {
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 1 && inputName.length() <= MAX_LEN + 1) {
            // 获取库
            List<String> specials = Tools.specials2List(CF_STR);
            // 对特殊字符串集合乱序
            Collections.shuffle(specials);
            // 输入字符处理
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> names = inputNames.stream().map(name -> Tools.string2Unicode(name)).collect(Collectors.toList());
            List<String> newSpecials = Tools.getStringByRadom(specials, MAX_LEN + 1 - inputName.length());
            // 对特殊字符串集合乱序
            Collections.shuffle(newSpecials);
            return Tools.positionChange(names, newSpecials);
        } else {
            throw new RuntimeException("输入的字数在1-6位，超过6个字无法更改");
        }

    }

    /**
     * 10.英文名串源
     * 例如2个英文 gg 两个英文算一个字  系统自动随机从数据库中生成5个进去随机顺序随机
     * 如果ggg 三个英文就是1个半 字自动视为两个 添加4个字符进去
     * 如果gggg 也视为2个 自动添加4个字符
     * ggggg五个 就视为三个字 自动添加三个
     * gggggg六个也是视为三个字 自动添加三个
     * 如果是十个就自动添加1个
     * 超过10个 就提示超长即可
     */
    private static String EN_STR = "\u200B\u2062\u2060\u2061\u2060\u2062\u2063\u2060\u2062\u2060\u2061\u2063\u2060\u2060\u2060\u2063\u2061\u200B\u2060\u200B\u2063\u2060\u2063\u200B\u2062\u2061\u2062\u2060\u2060\u2063\u2061\u2062\u2060\u2060\u2060\u2063\u200B\u2060\u2060\u2063\u2060\u2060\u2062\u2062\u2062\u2062\u2062\u2060\u2060\u2062\u2062\u200B\u2063\u2062\u2062\u2060\u2060\u200B\u200B\u2060\u2063\u2063\u2062\u2062\u2062\u2061\u2063\u2062\u200B\u2060\u2063\u2060\u200B\u2060\u2063\u2060\u200B\u2060\u2062\u2062\u2062\u2062\u200B\u200B\u2062\u2060\u2062\u200B\u200B\u2063\u2063\u2062\u200B\u2060\u2060\u2060\u2060\u2062\u2062\u2060\u2060\u2062\u2061\u2060\u200B\u2061\u2063\u2062\u200B\u2063\u2061\u200B\u2061\u200B\u200B\u200B\u200B\u200B\u200B\u200B\u2060\u200B\u200B\u200B\u200B\u2060\u2062\u2062\u2062\u200B\u2060\u2062\u2060\u200B\u200B\u2060\u2061\u2060\u2062\u2061\u2063\u2060\u2060\u200B\u2061\u2060\u200B\u2060\u2062\u2060\u2061\u2060\u2060\u2061\u2060\u2060\u2060\u2061\u2063\u2060";

    /**
     * 重复名
     *
     * @return
     */
    public static Map<String, String> generateNameByEN(String inputName) {
        // 输入字符处理
        if (StringUtils.isNotBlank(inputName) && inputName.length() >= 1 && inputName.length() <= MAX_LEN + 5) {
            // 获取库
            List<String> specials = Tools.specials2List(EN_STR);
            List<String> inputNames = Arrays.asList(inputName.split(""));
            List<String> tempUnicodeNames = new ArrayList<>();
            for (String input : inputNames) {
                tempUnicodeNames.add(Tools.string2Unicode(input));
            }
            // 对特殊字符串集合乱序
            Collections.shuffle(specials);
            int len;
            if (inputNames.size() % 2 == 0) {
                len = inputNames.size() / 2;
            } else {
                len = (inputNames.size() / 2) + 1;
            }
            List<String> newSpecials = Tools.getStringByRadom(specials, MAX_LEN + 1 - len);
            // 对特殊字符串集合乱序
            Collections.shuffle(newSpecials);
            return Tools.positionChange(tempUnicodeNames, newSpecials);
        } else {
            throw new RuntimeException("英文数字长度在1-10之间");
        }
    }

}
