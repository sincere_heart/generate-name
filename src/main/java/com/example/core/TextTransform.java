package com.example.core;

/**
 * @author liheng
 * @ClassName TextTransform
 * @Description
 * @date 2021-07-31 19:23
 */
public class TextTransform {
    private String origin;
    private String target;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
