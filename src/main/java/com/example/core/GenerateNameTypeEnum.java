package com.example.core;

import lombok.Getter;

/**
 * @author liheng
 * @ClassName GenerateNameTypeEnum
 * @Description
 * @date 2021-07-31 17:16
 */
public enum GenerateNameTypeEnum {
    PG_1_5_NAME_H(1, "1-5个字换字难改苹果"),
    AZ_1_5_NAME_H(2, "1-5个字难改换字安卓"),
    AZ_1_5_NAME(3, "1-5个字难改名字安卓"),
    PG_1_5_NAME(4, "1-5个字难改名字苹果"),
    SIX_NAME(5, "6个字名字"),
    AZ_GZ(6, "安卓贵族居中"),
    PG_GZ(7, "苹果贵族居中"),
    KB(8, "空白名"),
    CF(9, "重复名"),
    EN(10, "英文名"),
    ;
    @Getter
    private int code;
    @Getter
    private String desc;

    GenerateNameTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 通过code获取枚举
     *
     * @param code
     * @return
     */
    public static Object fromCode(Integer code, String input) {
        GenerateNameTypeEnum[] resultTypes = GenerateNameTypeEnum.values();
        for (GenerateNameTypeEnum resultType : resultTypes) {
            if (code.equals(resultType.getCode())) {
                switch (code){
                    case 1:
                        return GenerateNameTools.generateNameByPG_1_5_NAME_H(input);
                    case 2:
                        return GenerateNameTools.generateNameByAZ_1_5_NAME_H(input);
                    case 3:
                        return GenerateNameTools.generateNameByAZ_1_5_NAME(input);
                    case 4:
                        return GenerateNameTools.generateNameByPG_1_5_NAME(input);
                    case 5:
                        return GenerateNameTools.generateNameBySIX_NAME(input);
                    case 6:
                        return GenerateNameTools.generateNameByAZ_GZ();
                    case 7:
                        return GenerateNameTools.generateNameByPG_GZ();
                    case 8:
                        return GenerateNameTools.generateNameByKB();
                    case 9:
                        return GenerateNameTools.generateNameByCF(input);
                    case 10:
                        return GenerateNameTools.generateNameByEN(input);
                }
                return resultType;
                }
        }
        throw new RuntimeException("错误的调用类型");
    }
}
