package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.model.BackGround;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author liheng
 * @ClassName BackGroundMapper
 * @Description
 * @date 2021-07-29 17:40
 */
@Mapper
public interface BackGroundMapper extends BaseMapper<BackGround> {
}

