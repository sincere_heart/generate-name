package com.example.exception;


import com.example.swagger.GlobalResultEnum;

/**
 * 业务异常
 *
 * @author liheng
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -232388151980021793L;

    /**
     * 自定义异常码
     */
    private int code = GlobalResultEnum.FAIL.getCode();

    private Object data;

    public ServiceException() {
        super();
    }

    /**
     * 将定义的异常信息赋给异常
     *
     * @param code
     * @param msg
     */
    public ServiceException(int code, String msg) {
        this(code, msg, null);
    }

    public ServiceException(GlobalResultEnum code) {
        this(code, null);
    }

    public ServiceException(GlobalResultEnum code, Object o) {
        super(code.getMessage());
        this.code = code.getCode();
        this.data = o;
    }

    public ServiceException(int code, String msg, Object o) {
        super(msg);
        this.code = code;
        this.data = o;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
