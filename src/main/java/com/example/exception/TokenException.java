package com.example.exception;


import com.example.swagger.GlobalResultEnum;

/**
 * <p>token 异常</p>
 *
 * @author liheng
 * @date 2020/9/27 18:24
 */
public class TokenException extends RuntimeException {

    private static final long serialVersionUID = -232388151980021793L;


    /**
     * 自定义异常码
     */
    private int code = GlobalResultEnum.TOKEN_EXPIRE.getCode();

    private Object data;

    public TokenException(GlobalResultEnum code) {
        this(code, null);
    }

    public TokenException(GlobalResultEnum code, Object o) {
        super(code.getMessage());
        this.code = code.getCode();
        this.data = o;
    }

    public TokenException(int code, String msg, Object o) {
        super(msg);
        this.code = code;
        this.data = o;
    }

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenException(String message) {
        super(message);
    }

    public TokenException(Throwable cause) {
        super(cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
