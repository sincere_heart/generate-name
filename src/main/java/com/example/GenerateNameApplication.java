package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liheng
 */
@SpringBootApplication
@MapperScan({
        "com.example.mapper"
})
public class GenerateNameApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenerateNameApplication.class, args);
    }

}
