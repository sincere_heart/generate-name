package com.example.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liheng
 * @since 2021-05-20
 */
@Data
@TableName("back_ground")
public class BackGround  implements Serializable {

    private static final long serialVersionUID = 8310260268480860187L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("id,修改必传")
    private Integer id;

    @TableField("background_url")
    @NotBlank(message = "路径，获取是 加接口地址拼接")
    @Length(max = 200, message = "不超过200个字符")
    @ApiModelProperty("背景图地址")
    private String shortName;
}
