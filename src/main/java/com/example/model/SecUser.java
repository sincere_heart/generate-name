package com.example.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.pojo.BaseModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author liheng
 * @since 2021-05-20
 */
@Data
@TableName("sec_user")
public class SecUser extends BaseModel implements Serializable {

    private static final long serialVersionUID = 8310260268480860187L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("id,修改必传")
    private Integer id;

    @TableField("short_name")
    @NotBlank(message = "姓名(简称)不能为空")
    @Length(max = 32, message = "姓名(简称)长度不能超过32个字符")
    @ApiModelProperty("账号描述")
    private String shortName;

    @TableField("account")
    @ApiModelProperty("登陆账号")
    @NotBlank(message = "登陆账号不能为空")
    @Length(max = 12, message = "登陆账号长度不能超过12个字符")
    @Pattern(regexp = "^[A-Za-z0-9]{2,12}$",message = "账号由英文、数字组成，并且长度为2-12位")
    private String account;

    @TableField("password")
    @Length(max = 16, message = "密码长度不能超过16个字符")
    @ApiModelProperty("密码")
    @Pattern(regexp = "^[a-z_A-Z0-9-\\.!@#\\$%\\\\\\^&\\*\\)\\(\\+=\\{\\}\\[\\]\\/\",'<>~\\·`\\?:;|]{6,16}$",message = "密码由英文、数字、符号组成，且长度为6-16位")
    private String password;

}
