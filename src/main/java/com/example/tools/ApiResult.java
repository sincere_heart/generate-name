package com.example.tools;

import com.example.swagger.GlobalResultEnum;

import java.io.Serializable;

/**
 * 前后端交互对象
 *
 * @author madman
 */
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = 5870642374421413864L;

    private static final ApiResult<String> SUCCESS_RESULT = new ApiResult<>();

    private static final ApiResult<String> ERROR_RESULT = new ApiResult<>(GlobalResultEnum.FAIL.getCode(), false, GlobalResultEnum.FAIL.getMessage());

    private int code = GlobalResultEnum.SUCCESS.getCode();

    /**
     * 是否成功，默认true
     */
    private boolean success = Boolean.TRUE;
    /**
     * 提示信息，默认"操作成功"
     */
    private String msg = GlobalResultEnum.SUCCESS.getMessage();
    /**
     * 返回数据
     */
    private T data;

    public ApiResult() {
        this.setMsg(msg);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ApiResult(T data) {
        this.data = data;
    }


    public ApiResult(String msg) {
        this.msg = msg;
    }


    public ApiResult(int code, boolean success, String msg) {
        this.code = code;
        this.success = success;
        this.msg = msg;
    }


    public ApiResult(int code, boolean success, String msg, T data) {
        this.code = code;
        this.success = success;
        this.msg = msg;
        this.data = data;
    }

    public static ApiResult<String> ok() {
        return SUCCESS_RESULT;
    }

    public static <T> ApiResult<T> ok(T data) {
        return new ApiResult<>(data);
    }

    public static ApiResult okMsg(String message) {
        return new ApiResult<>(message);
    }

    public static ApiResult<String> failed() {
        return ERROR_RESULT;
    }

    public static ApiResult<String> failed(String message) {
        return new ApiResult<>(GlobalResultEnum.FAIL.getCode(), false, message);
    }

    public static <T> ApiResult<T> failed(T data) {
        return new ApiResult<>(data);
    }

    public static <T> ApiResult<T> failed(int code, String message) {
        return new ApiResult<>(code, false, message);
    }

    public static <T> ApiResult<T> failed(int code, String message, T data) {
        return new ApiResult<>(code, false, message, data);
    }

    public static <T> ApiResult<T> status(GlobalResultEnum status) {
        return status(status, null);
    }




    public static <T> ApiResult<T> status(GlobalResultEnum status, T data) {
        return new ApiResult<>(status.getCode(), status.getCode() == GlobalResultEnum.SUCCESS.getCode(), status.getMessage(), data);
    }
}
