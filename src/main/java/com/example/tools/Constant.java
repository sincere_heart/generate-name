package com.example.tools;


/**
 * @author liheng
 * @ClassName Constant
 * @Description
 * @date 2020-08-20 17:19
 */

public class Constant {
    /**
     * 管理员
     */
    public static final String ADMIN = "admin";

    /**
     * 默认密码
     */
    public static final String DEF_PASSWORD = "123456";

}
