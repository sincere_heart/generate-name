package com.example.tools;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author lihen
 */
@Slf4j
public class ResponseUtils {

    /**
     * 往 response 写出 json
     * @param response
     * @param data
     */
    public static void renderJson(HttpServletResponse response, String data) {
        try {
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter printWriter = response.getWriter();
            printWriter.write(data);
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        } catch (IOException ex) {
            log.error("Response写出JSON异常，", ex);
        }
    }

    /**
     * 往 response 写出 json
     *
     * @param response 响应
     * @param result   返回数据
     */
    public static <T> void renderJson(HttpServletResponse response, ApiResult<T> result) {
        try {
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter printWriter = response.getWriter();
            printWriter.write(JsonUtils.toJsonString(result));
            if (printWriter != null) {
                printWriter.flush();
                printWriter.close();
            }
        } catch (IOException ex) {
            log.error("Response写出JSON异常，", ex);
        }
    }
}
