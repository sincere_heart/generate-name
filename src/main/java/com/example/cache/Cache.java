package com.example.cache;

import java.util.concurrent.TimeUnit;

/**
 * 缓存
 *
 * @author liheng
 */
public class Cache {
    /**
     * 缓存的初始化容量
     */
    private int initialCapacity = 50;
    /**
     * 缓存最大容量
     */
    private long maximumSize = 200L;
    /**
     * 缓存时长
     */
    private long duration = 7000L;
    /**
     * 时长单位,自动转换
     * 支持：
     * 时
     * 分
     * 秒
     * 天
     */
    private TimeUnit timeunit = TimeUnit.SECONDS;

    public int getInitialCapacity() {
        return initialCapacity;
    }

    public void setInitialCapacity(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    public long getMaximumSize() {
        return maximumSize;
    }

    public void setMaximumSize(long maximumSize) {
        this.maximumSize = maximumSize;
    }


    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public TimeUnit getTimeunit() {
        return timeunit;
    }

    public void setTimeunit(TimeUnit timeunit) {
        this.timeunit = timeunit;
    }

    public static class Builder {
        private int initialCapacity;
        private long maximumSize;
        private long duration;
        private TimeUnit timeunit;

        public Builder setInitialCapacity(int initialCapacity) {
            this.initialCapacity = initialCapacity;
            return this;
        }

        public Builder setMaximumSize(long maximumSize) {
            this.maximumSize = maximumSize;
            return this;
        }

        public Builder setDuration(long duration) {
            this.duration = duration;
            return this;
        }

        public Builder setTimeUnit(TimeUnit timeunit) {
            this.timeunit = timeunit;
            return this;
        }

        public Cache build() {
            return new Cache(this);
        }
    }

    public static Builder options() {
        return new Builder();
    }

    private Cache(Builder builder) {
        this.initialCapacity = 0 == builder.initialCapacity ? this.initialCapacity : builder.initialCapacity;
        this.maximumSize = 0L == builder.maximumSize ? this.maximumSize : builder.maximumSize;
        this.duration = 0L == builder.duration ? this.duration : builder.duration;
        this.timeunit = null == builder.timeunit ? this.timeunit : builder.timeunit;
    }

    @Override
    public String toString() {
        return "Cache{" +
                "initialCapacity=" + initialCapacity +
                ", maximumSize=" + maximumSize +
                ", duration=" + duration +
                ", timeunit=" + timeunit +
                '}';
    }
}
