package com.example.cache;

import com.github.benmanes.caffeine.cache.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * @Author li heng
 * @Description token本地缓存，使用caffine缓存实现
 * @Date 2019/6/1 17:28
 * @Version 1.0
 */
@Slf4j
public class CaffineCacheManage {

    /**
     * 缓存管理
     *
     * @param cache
     * @param <T>
     * @return
     */
    public static <T> LoadingCache<String, T> caffineCacheManage(Cache cache) {
        log.info("初始化缓存的实体数据：{}", cache);
        if (Objects.isNull(cache)) {
            throw new NullPointerException("请实例化一个Cache对象!");
        }
        LoadingCache<String, T> localcache =
                // 构建本地缓存，调用链的方式
                // ,1000是设置缓存的初始化容量，maximumSize是设置缓存最大容量，当超过了最大容量，guava将使用LRU算法（最少使用算法），来移除缓存项
                // expireAfterAccess(12,TimeUnit.HOURS)设置缓存有效期为12个小时
                Caffeine.newBuilder().initialCapacity(cache.getInitialCapacity()).maximumSize(cache.getMaximumSize())
                        // 设置写缓存后n秒钟过期
                        // .expireAfterWrite(30, TimeUnit.SECONDS)
                        .expireAfterWrite(cache.getDuration(), cache.getTimeunit())
                        // 设置读写缓存后n秒钟过期,实际很少用到,类似于expireAfterWrite
                        //.expireAfterAccess(googleCache.getDuration(), googleCache.getTimeunit())
                        // 只阻塞当前数据加载线程，其他线程返回旧值
                        //.refreshAfterWrite(10, TimeUnit.SECONDS)
                        // 设置缓存的移除通知//用户手动移除EXPLICIT,
                        // //用户手动替换REPLACED,//被垃圾回收COLLECTED,//超时过期EXPIRED,//SIZE由于缓存大小限制
                        .removalListener(new RemovalListener<String, T>() {
                            @Override
                            public void onRemoval(String key, Object value, RemovalCause cause) {
                                log.info(key + ":" + value + ":" + cause.name());
                            }
                        })
                        // build里面要实现一个匿名抽象类
                        .build(new CacheLoader<String, T>() {
                            // 这个方法是默认的数据加载实现,get的时候，如果key没有对应的值，就调用这个方法进行加载。此处是没有默认值则返回null
                            @Override
                            public T load(String key) throws Exception {
                                return null;
                            }
                        });
        return localcache;
    }

}
