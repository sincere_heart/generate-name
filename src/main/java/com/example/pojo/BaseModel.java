package com.example.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liheng
 * @ClassName BaseModel
 * @Description
 * @date 2021-04-02 19:38
 */
@Data
public class BaseModel implements Serializable {
    /**
     * 字段常量属性
     */
    public static final String ID = "id";

    public static final String CREATE_TIME = "createtime";

    public static final String LAST_TIME = "lasttime";

    private static final long serialVersionUID = 2553749188490103197L;
    /**
     * 删除  未删除
     */
    @JsonIgnore
    @TableField("`enable`")
    @TableLogic
    private Boolean enable;

    @ApiModelProperty(value = "记录创建时间，前端忽略")
    @TableField("createtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createtime;

    /**
     * 最后修改时间
     */
    @JsonIgnore
    @TableField("lasttime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime lasttime;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public LocalDateTime getCreatetime() {
        return createtime;
    }

    public void setCreatetime(LocalDateTime createtime) {
        this.createtime = createtime;
    }

    public LocalDateTime getLasttime() {
        return lasttime;
    }

    public void setLasttime(LocalDateTime lasttime) {
        this.lasttime = lasttime;
    }
}
