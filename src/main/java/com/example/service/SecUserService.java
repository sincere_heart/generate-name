package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dto.SecUserDTO;
import com.example.model.SecUser;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liheng
 * @since 2021-05-20
 */
public interface SecUserService extends IService<SecUser> {

    /**
     * 新增管理员账号
     *
     * @param dto 管理员信息
     * @return 新增结果
     */
    void addOrUpdateUser(SecUserDTO dto);
}
