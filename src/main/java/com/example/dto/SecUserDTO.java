package com.example.dto;

import com.example.model.SecUser;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/3 10:59
 */
@Data
@ApiModel("账户DTO")
public class SecUserDTO extends SecUser {
}
