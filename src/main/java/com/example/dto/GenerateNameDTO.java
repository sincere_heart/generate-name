package com.example.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liheng
 * @ClassName GenerateNameDTO
 * @Description
 * @date 2021-07-29 14:27
 */
@Data
@ApiModel("名称生成DTO")
public class GenerateNameDTO {
    // 输入框的字数确认，输入框名字确认
    @ApiModelProperty("除不需要nickName的其他都需要传" +
            "    (1, 2-5个字换字难改苹果),\n" +
            "    (2, 2-5个字难改换字安卓),\n" +
            "    (3, 1-5个字难改名字安卓),\n" +
            "    (4, 1-5个字难改名字苹果),\n" +
            "    (5, 6个字名字（这个会返回一个数组结构）),\n" +
            "    (6, 安卓贵族居中（不需要nickName）),\n" +
            "    (7, 苹果贵族居中（不需要nickName）),\n" +
            "    (8, 空白名（不需要nickName）),\n" +
            "    (9, 1-6重复名),\n" +
            "    (10,1-10位英文名，可包含数字),")
    private Integer code;
    @ApiModelProperty("nickName不传")
    private String nickName;
}
