package com.example.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/17 14:20
 */
@Data
@ApiModel("密码重置或修改DTO")
public class ResetPasswordDTO {
    private String password;
    private Integer id;
}
