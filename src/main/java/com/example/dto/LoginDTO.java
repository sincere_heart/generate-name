package com.example.dto;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;

/**
 * @author liheng
 * @ClassName LoginDto
 * @Description
 * @date 2020-08-20 17:52
 */
@Data
@ApiModel("登录DTO")
public class LoginDTO {
    @NotBlank(message = "账号不能为空")
    private String account;

    @NotBlank(message = "密码不能为空")
    private String password;
}
