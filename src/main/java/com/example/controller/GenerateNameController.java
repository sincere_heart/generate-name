package com.example.controller;

import com.example.annotation.UserAuth;
import com.example.core.GenerateNameTypeEnum;
import com.example.dto.GenerateNameDTO;
import com.example.tools.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author liheng
 * @ClassName GenerateNameController
 * @Description
 * @date 2021-07-29 14:26
 */
@Slf4j
@Api(tags = "名字生成相关接口")
@RestController
@RequestMapping("/")
@CrossOrigin
@UserAuth
public class GenerateNameController {

    @ApiOperation("生成名称")
    @PostMapping("generate-name")
    public ApiResult generateName(@RequestBody GenerateNameDTO dto) {
        return ApiResult.ok( GenerateNameTypeEnum.fromCode(dto.getCode(),dto.getNickName()));
    }
}
