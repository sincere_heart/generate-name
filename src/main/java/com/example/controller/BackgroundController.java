package com.example.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.annotation.UserAuth;
import com.example.core.Tools;
import com.example.dto.GenerateNameDTO;
import com.example.mapper.BackGroundMapper;
import com.example.model.BackGround;
import com.example.tools.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author liheng
 * @ClassName BackgroundController
 * @Description
 * @date 2021-07-29 17:34
 */
@Slf4j
@Api(tags = "背景图相关接口")
@RestController
@RequestMapping("/")
@CrossOrigin
@UserAuth
public class BackgroundController {
    @Autowired
    private BackGroundMapper backGroundMapper;

    @ApiOperation("上传背景图")
    @PostMapping("background")
    public ApiResult<String> background(@RequestBody BackGround dto) {
        if (Objects.isNull(dto.getId())) {
            backGroundMapper.insert(dto);
        } else {
            backGroundMapper.updateById(dto);
        }
        return ApiResult.ok();
    }

    @ApiOperation("拉取背景图")
    @GetMapping("load")
    public ApiResult<BackGround> load() {
        return ApiResult.ok(backGroundMapper.selectOne(Wrappers.lambdaQuery(BackGround.class).orderByDesc(BackGround::getId).last(" limit 1")));
    }
}
