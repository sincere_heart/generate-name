'use strict';

// 创建axios
let _axios = axios.create({
  baseURL: 'http://106.54.165.67:8080',
  timeout: 10000,
});
_axios.interceptors.request.use(function(config){
  //比如是否需要设置 token
  config.headers.token = localStorage.getItem('token');
  return config
})
_axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  if (response.status === 200) {
    return response.data;
  } else {
    return response;
  }
}, function (error) {
  console.log('请求错误信息', error);
  return Promise.reject(error);
});

