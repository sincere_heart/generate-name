package com.example;

import com.example.config.DataJsonConfig;
import com.example.core.GenerateNameTools;
import com.example.core.SpecialCharacterExcel;
import com.example.core.Tools;
import com.example.tools.JsonUtils;
import com.google.common.base.Joiner;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@SpringBootTest
class GenerateNameApplicationTests {

//    @Test
//    void contextLoads() {
//        String filePath = "C:\\Users\\lihen\\Desktop\\321.xls";
//        List<SpecialCharacterExcel> apiInfos = Tools.readExcel(filePath, 0, SpecialCharacterExcel.class);
//        List<String> symbols;
//        List<String> result;
//        String input = "我";
//        List<String> news;
//        String inputUni = Tools.stringToUnicode(input);
//        for (SpecialCharacterExcel a12 : apiInfos) {
//            System.out.println(a12.getAa());
//            news = new ArrayList<>();
//            news.add(inputUni);
//            String ww = Tools.stringToUnicode(a12.getAa());
//            String ss = ww.replaceAll("\\\\u200B", "").replaceAll("\\\\u200b", "").replaceAll("\\\\u2060", "");
//            ss = ss.replaceAll("\\\\u", "\\\\u0020\\\\u").replaceFirst("\\\\u0020", "");
//            symbols = Arrays.asList(ss.split("\\\\u0020"));
//            result = Tools.getStringByRadom(symbols, 6);
//            news.addAll(result);
//            Collections.shuffle(news);
//            System.out.println(Tools.unicodeToString(Joiner.on("").join(news)));
//        }
//    }

//
//    @Test
//    void contextLoads3() {
//        System.out.println(GenerateNameTools.generateNameBySIX_NAME( "住身向我的天").toString());
//    }
//
//

    @Test
    void contextLoads2() {
        String filePath = "C:\\Users\\lihen\\Desktop\\123.xls";
        List<SpecialCharacterExcel> excels = Tools.readExcel(filePath, SpecialCharacterExcel.class);
        Map<String, String> map = new HashMap<>();
        String rgex = "(?<=【).*?(?=】)";
//        String rgex =  "(【).*?(】)";
        Pattern p = Pattern.compile(rgex); //编译对象
        Matcher m;
        int first;
        int last;
        String str;
        for (SpecialCharacterExcel excel : excels) {
            first = excel.getA1().lastIndexOf("】") + 1;
            last = excel.getA1().length();
            str = excel.getA1().substring(first, last);
            m = p.matcher(excel.getA1()); //进行匹配
            if (m.find()) {
                map.put(str, m.group());
            }

            first = excel.getA2().lastIndexOf("】") + 1;
            last = excel.getA2().length();
            str = excel.getA2().substring(first, last);
            m = p.matcher(excel.getA2()); //进行匹配
            if (m.find()) {
                map.put(str, m.group());
            }
            first = excel.getA3().lastIndexOf("】") + 1;
            last = excel.getA3().length();
            str = excel.getA3().substring(first, last);
            m = p.matcher(excel.getA3()); //进行匹配
            if (m.find()) {
                map.put(str, m.group());
            }
            first = excel.getA4().lastIndexOf("】") + 1;
            last = excel.getA4().length();
            str = excel.getA4().substring(first, last);
            m = p.matcher(excel.getA4()); //进行匹配
            if (m.find()) {
                map.put(str, m.group());
            }
            System.out.println(">>>:"+str);
        }
        System.out.println(JsonUtils.toJsonString(map));
    }
}
